/* Author- Bunmi Abioye
Program - Computer Applications Development
 */

// server side programming
// importing dependencies
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const {check, validationResult} = require('express-validator'); 

// setting up global variables
var myApp = express();
myApp.use(bodyParser.urlencoded({ extended:false }));

// setting path to public folders and view folders
myApp.set('views', path.join(__dirname, 'views'));

// setting path to public folders (css, js, images)
myApp.use(express.static(__dirname+'/public'));

// define view engine (ejs)
myApp.set('view engine', 'ejs');

// handling the HTTP requests
//Constants - cost of each product
const beakersCost = 4;
const bottlesCost = 3;
const microscopeCost = 500;
const thermometerCost = 100;
const incubatorCost = 300;

//Constants - tax for each province
const AlbertaVAT = 0.05;
const BritishColumbiaVAT = 0.05;
const ManitobaVAT = 0.05;
const NewBrunswickVAT = 0.15;
const NewfoundlandAndLabradorVAT = 0.15;
const NorthwestTerritoriesVAT = 0.05;
const NovaScotiaVAT = 0.15;
const NunavutVAT = 0.05;
const OntarioVAT = 0.13;
const PrinceEdwardIslandVAT = 0.15;
const QuebecVAT = 0.14975;
const SaskatchewanVAT = 0.05;
const YukonVAT = 0.05;

// expressions
var postCodeRegex = /^[A-Za-z][0-9][A-Za-z][\s]?[0-9][A-Za-z][0-9]$/;
var phoneRegex = /^[0-9]{3}[\s\-]?[0-9]{3}[\s\-]?[0-9]{4}$/;

// loading the index file
myApp.get('/', function(req,res){
    res.render('index');
});

// getthing the cost of products on the loaded file
var priceList = {
    microscopeCost : microscopeCost,
    thermometerCost : thermometerCost,
    incubatorCost : incubatorCost,
    beakersCost : beakersCost,
    bottlesCost : bottlesCost,
}
myApp.get('/order', function(req,res){
    res.render('order', priceList);
});

// Sign up page submission handler
myApp.post('/', [
    check('firstName', 'Your first name is required!').notEmpty(),
    check('lastName', 'Your last name is required!').notEmpty(),
    check('streetNumber', 'Your street number is required!').notEmpty(),
    check('streetNumber', 'Your street number must be a number!').isNumeric(),
    check('streetName', 'Your street name is required!').notEmpty(),
    check('postCode', 'Your postal code is not in the correct format!').matches(postCodeRegex),
    check('city', 'Your city is required!').notEmpty(),
    check('province', 'Your province is required!').notEmpty(),
    check('email', 'Your email address is not in the correct format!').isEmail(),
    check('password', 'Your password must be at least 5 letters long!').isLength({ min: 5 }),
    check('phone', 'Your phone number is not in the correct format!').matches(phoneRegex)
], function(req, res){
    const errors = validationResult(req);
    if (!errors.isEmpty())
    {
        res.render('index', {
            errors:errors.array()
        });
    }
    else
    {       
        var firstName = req.body.firstName;
        var lastName = req.body.lastName;
        var streetNumber = req.body.streetNumber;
        var streetName = req.body.streetName;
        var postCode = req.body.postCode;
        var city = req.body.city;
        var province = req.body.province;
        var email = req.body.email;
        var password = req.body.password;
        var phone = req.body.phone;

        var pageData = {
            firstName : firstName,
            lastName : lastName,
            streetNumber : streetNumber,
            streetName : streetName,
            postCode : postCode,
            city : city,
            province : province,
            email : email,
            password : password,
            phone : phone
        }
        res.render('index', pageData); 
    }    
});

// Order page submission handler
myApp.post('/order', function(req,res){      
    var beakersQuantity = req.body.beakersQuantity;
    var bottlesQuantity = req.body.bottlesQuantity;
    var microscopeQuantity = req.body.microscopeQuantity;
    var thermometerQuantity = req.body.thermometerQuantity;
    var incubatorQuantity = req.body.incubatorQuantity;
    
    // the calculations
    var beakersPrice = beakersQuantity * beakersCost;
    var bottlesPrice = bottlesQuantity * bottlesCost;
    var microscopePrice = microscopeQuantity * microscopeCost;    
    var thermometerPrice = thermometerQuantity * thermometerCost;
    var incubatorPrice = incubatorQuantity * incubatorCost;

    var subTotal = beakersPrice + bottlesPrice + microscopePrice + thermometerPrice + incubatorPrice;

    //Computing tax by province
    var selectedProvince = req.body.selectedProvince;
    
    var Provinces = new Array (
        "Alberta",
        "BritishColumbia",
        "Manitoba",
        "NewBrunswick",
        "NewfoundlandAndLabrador",
        "NorthwestTerritories",
        "NovaScotia",
        "Nunavut",
        "Ontario",
        "PrinceEdwardIsland",
        "Quebec",
        "Saskatchewan",
        "Yukon"
    );       
    var VATs = new Array (
        AlbertaVAT,
        BritishColumbiaVAT,
        ManitobaVAT,
        NewBrunswickVAT,
        NewfoundlandAndLabradorVAT,
        NorthwestTerritoriesVAT,
        NovaScotiaVAT,
        NunavutVAT,
        OntarioVAT,
        PrinceEdwardIslandVAT,
        QuebecVAT,
        SaskatchewanVAT,
        YukonVAT,
    );
    
    for (i = 0; i < Provinces.length; i++)
    {
        if (selectedProvince == Provinces[i])
        {
            tax = VATs[i];
        }
        if (selectedProvince == '')
        {
            tax = 0;
        }
    };        
    //Calculations    
    var taxPercent = tax * 100;
    var taxCharged = tax * subTotal;
    var total = subTotal + taxCharged;

    var receiptData = {
        selectedProvince : selectedProvince,
        beakersCost : beakersCost,
        bottlesCost : bottlesCost,
        microscopeCost : microscopeCost,
        thermometerCost : thermometerCost,
        incubatorCost : incubatorCost,
        beakersQuantity : beakersQuantity,
        bottlesQuantity : bottlesQuantity,
        microscopeQuantity : microscopeQuantity,
        thermometerQuantity : thermometerQuantity,
        incubatorQuantity : incubatorQuantity,       
        beakersPrice : beakersPrice,
        bottlesPrice : bottlesPrice,
        microscopePrice : microscopePrice,
        thermometerPrice : thermometerPrice,
        incubatorPrice : incubatorPrice,
        subTotal : subTotal,
        taxPercent : taxPercent,
        taxCharged : taxCharged,
        total : total
    }
    res.render('order', receiptData);    
});

// start the server and listen to a port
myApp.listen(8080);
console.log('The server is actively working! Open http://localhost:8080');