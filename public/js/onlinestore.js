/* Author- Bunmi Abioye
Program - Computer Applications Development
Date November 20, 2020 */

//Regular expressions
var postCodeRegex = /^[A-Za-z][0-9][A-Za-z][\s]?[0-9][A-Za-z][0-9]$/;
var phoneRegex = /^[0-9]{3}[\s\-]?[0-9]{3}[\s\-]?[0-9]{4}$/;
var passwordRegex=  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
var emailRegex = /^[a-zA-Z0-9]+@[a-zA-Z0-9-]+(.[a-zA-Z0-9-]+)*$/;
var creditCardRegex = /^[0-9]{4}[\s\-]?[0-9]{4}[\s\-]?[0-9]{4}[\s\-]?[0-9]{4}$/;
//Unit costs (Constants)
beakersCost = 4;
bottlesCost = 3;
microscopeCost = 500;
thermometerCost = 100;
incubatorCost = 300;

//This function is to set cost for all products and call it up as the site loads
 function  Costs(){
    return true;
    document.getElementById('microscopeCost').innerHTML = `CAD $ ${microscopeCost.toFixed(2)}`;
    document.getElementById('thermometerCost').innerHTML = `CAD $ ${thermometerCost.toFixed(2)}`;
    document.getElementById('incubatorCost').innerHTML = `CAD $ ${incubatorCost.toFixed(2)}`;
    document.getElementById('beakersCost').innerHTML = `CAD $ ${beakersCost.toFixed(2)}`;
    document.getElementById('bottlesCost').innerHTML = `CAD $ ${bottlesCost.toFixed(2)}`;
}
window.onload = Costs;

// This function processes the data input for the sign up page, and validates
function formSubmit(){

    return true;

    var firstName = document.getElementById('firstName').value;
    var lastName = document.getElementById('lastName').value;
    var streetNumber = document.getElementById('streetNumber').value;
    var streetName = document.getElementById('streetName').value;
    var postCode = document.getElementById('postCode').value;
    var city = document.getElementById('city').value;
    var province = document.getElementById('province').value;
    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;
    var confirmPassword = document.getElementById('confirmPassword').value;
    var phone = document.getElementById('phone').value;
    var creditCard = document.getElementById('creditCard').value;

    //to validate input
    var errors = '';
    nextstep='';
    
    if(firstName == ''){
        errors += `Your first name is required! <br><br>`;
    }
    if(lastName == ''){
        errors += `Your last name is required! <br><br>`;
    }
    if(streetNumber == '' || isNaN(streetNumber) ){
        errors += `Your street number is required and must be a number! <br><br>`;
    }
    if(streetName == ''){
        errors += `Your street name is required for a complete address! <br><br>`;
    }
    if(!postCodeRegex.test(postCode)){
        errors += `Your postal code is not in the correct format! <br><br>`;
    }
    if(city == ''){
        errors += `Your city is required! <br><br>`;
    }
    if(province == ''){
        errors += `Your province is required! <br><br>`;
    }
    if(!emailRegex.test(email)){
        errors += `Your email address is not in the correct format! <br><br>`;
    }
    if(!passwordRegex.test(password)){
        errors += `Your password is not in the correct format. It should be at least 8 characters, comprising at least one lower case, one upper case and one number! <br><br>`;
    }
    if(confirmPassword != password){
        errors += `Your passwords are not the same! <br><br>`;
    }
    if(!phoneRegex.test(phone)){
        errors += `Your phone number is not in the correct format! <br><br>`;
    }
    if(!creditCardRegex.test(creditCard)){
        errors += `Your credit card number is not in the correct format! <br><br>`;
    }
    if(errors != ''){
        document.getElementById('errors').innerHTML = errors;
    }
    else{
         document.getElementById('errors').innerHTML = '';
         nextstep += 'Congratulations!!! <br> You have successfully signed up! <br> Please proceed to place your order.'
         document.getElementById('nextstep').innerHTML=nextstep;
         document.getElementById('nextstep').style.color="#006400";
    }   
    return false; 
}
//This function computes the sales values and generates the receipt
function checkOutPage (){

    return true;

    document.getElementById('receipt').innerHTML = '';
    // the inputs from the users
    var microscopeQuantity = document.getElementById('microscopeQuantity').value;
    var thermometerQuantity = document.getElementById('thermometerQuantity').value;
    var incubatorQuantity = document.getElementById('incubatorQuantity').value;
    var beakersQuantity = document.getElementById('beakersQuantity').value;
    var bottlesQuantity = document.getElementById('bottlesQuantity').value;

    // the calculations
    var microscopePrice = microscopeQuantity * microscopeCost;    
    var thermometerPrice = thermometerQuantity * thermometerCost;
    var incubatorPrice = incubatorQuantity * incubatorCost;
    var beakersPrice = beakersQuantity * beakersCost;
    var bottlesPrice = bottlesQuantity * bottlesCost;
        
    receiptSummary = '';
    if (microscopePrice > 0){
        receiptSummary += `Microscope (${microscopeQuantity}): CAD $ ${microscopePrice.toFixed(2)}<br>`;
    }
    if (thermometerPrice > 0){
        receiptSummary += `Thermometer (${thermometerQuantity}): CAD $ ${thermometerPrice.toFixed(2)}<br>`;
    }
    if (incubatorPrice > 0){
        receiptSummary += `Incubator (${incubatorQuantity}): CAD $ ${incubatorPrice.toFixed(2)}<br>`;
    }
    if (beakersPrice > 0){
        receiptSummary += `Beakers (${beakersQuantity}): CAD $ ${beakersPrice.toFixed(2)}<br>`;
    }
    if (bottlesPrice > 0){
        receiptSummary += `Bottles (${bottlesQuantity}): CAD $ ${bottlesPrice.toFixed(2)}<br>`;
    }
    if (receiptSummary != null){
        document.getElementById('receiptSummary').innerHTML = receiptSummary;
    }
    else {
        document.getElementById('receiptSummary').innerHTML = '';
    }
    var SubTotal = microscopePrice + thermometerPrice + incubatorPrice + beakersPrice + bottlesPrice;
    receipt = '';
    receipt += `Subtotal: CAD $ ${SubTotal.toFixed(2)}`;
    document.getElementById('receipt').innerHTML = receipt;

    // Validation for minimum order quantity
    lowPurchase = '';
    thanks = '';
    
    if (SubTotal <= 0){
        lowPurchase += `A receipt will not be generated, please make an order of $10 and above!`;
    }
    else if (SubTotal < 10){
        lowPurchase += `A receipt will not be generated as your transaction cannot be completed! <br> Your purchase is less than the $10 minimum order purchase, please increase your purchase to $10 or more.`;
    }
    if (lowPurchase != ''){    
        document.getElementById('lowPurchase').innerHTML = lowPurchase;
    }
    else {
        document.getElementById('lowPurchase').innerHTML = '';
        thanks += 'Thanks for shopping with us!'
        document.getElementById('thanks').innerHTML=thanks;
        document.getElementById('thanks').style.color="#006400";
    }   

    //Computing tax by province
    var selectedProvince = document.getElementById('selectedProvince').value;
    //Constants - tax for each province
    AlbertaVAT = 0.05;
    BritishColumbiaVAT = 0.05;
    ManitobaVAT = 0.05;
    NewBrunswickVAT = 0.15;
    NewfoundlandAndLabradorVAT = 0.15;
    NorthwestTerritoriesVAT = 0.05;
    NovaScotiaVAT = 0.15;
    NunavutVAT = 0.05;
    OntarioVAT = 0.13;
    PrinceEdwardIslandVAT = 0.15;
    QuebecVAT = 0.14975;
    SaskatchewanVAT = 0.05;
    YukonVAT = 0.05;

    if (selectedProvince == 'Alberta'){
        tax = AlbertaVAT;
    }
    if (selectedProvince == 'BritishColumbia'){
        tax = BritishColumbiaVAT;
    }
    if (selectedProvince == 'Manitoba'){
        tax = ManitobaVAT;
    }
    if (selectedProvince == 'NewBrunswick'){
        tax = NewBrunswickVAT;
    }
    if (selectedProvince == 'NewfoundlandAndLabrador'){
        tax = NewfoundlandAndLabradorVAT;
    }
    if (selectedProvince == 'NorthwestTerritories'){
        tax = NorthwestTerritoriesVAT;
    }
    if (selectedProvince == 'NovaScotia'){
        tax = NovaScotiaVAT;
    }
    if (selectedProvince == 'Nunavut'){
        tax = NunavutVAT;
    }
    if (selectedProvince == 'Ontario'){
        tax = OntarioVAT;
    }
    if (selectedProvince == 'PrinceEdwardIsland'){
        tax = PrinceEdwardIslandVAT;
    }
    if (selectedProvince == 'Quebec'){
        tax = QuebecVAT;
    }
    if (selectedProvince == 'Saskatchewan'){
        tax = SaskatchewanVAT;
    }
    else if (selectedProvince == 'Yukon'){
        tax = YukonVAT;
    }
     //Province validation
    provinceTax = '';
    if (selectedProvince == ''){
        provinceTax += `A receipt will not be generated as your transaction cannot be completed! <br> Please select your province from the dropdown on top of this page, in order to complete the transaction.<br>`;
    }
    if (provinceTax != ''){    
        document.getElementById('provinceTax').innerHTML = provinceTax;
        document.getElementById('thanks').innerHTML='';
    }
    else {
        document.getElementById('provinceTax').innerHTML = '';
    }
    //Completion of tax calculation
    var taxPercent = tax * 100;
    var taxCharged = tax * SubTotal;
    VAT = '';
    VAT += `VAT (${taxPercent}%): CAD $ ${taxCharged.toFixed(2)}`;
    document.getElementById('VAT').innerHTML = VAT;
    //Completion of the calculation for sales for the receipt
    var total = SubTotal + taxCharged;
    totalCost = '';
    totalCost += `Total: CAD $ ${total.toFixed(2)}`;
    document.getElementById('totalCost').innerHTML=totalCost;
    
    return false; 
}